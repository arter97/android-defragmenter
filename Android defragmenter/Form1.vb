﻿Imports Microsoft.Win32
Imports Microsoft.WindowsAPICodePack
Imports Microsoft.WindowsAPICodePack.Taskbar
Imports System.Threading
Imports System.Security.Cryptography
Imports System.Security.Principal
Imports System.Text
Imports System.IO

Public Class Form1

    Sub Delay(ByVal dblSecs As Double)
        If dblSecs = 0 Then
            Application.DoEvents()
            Thread.Sleep(1)
            Exit Sub
        End If
        Const OneSec As Double = 1.0# / (1440.0# * 60.0#)
        Dim dblWaitTil As Date
        Now.AddSeconds(OneSec)
        dblWaitTil = Now.AddSeconds(OneSec).AddSeconds(dblSecs)
        Do Until Now > dblWaitTil
            Application.DoEvents()
            Thread.Sleep(5)
        Loop
    End Sub

    Public osver As Integer = My.Computer.Info.OSVersion.Substring(0, 3).Replace(".", String.Empty)
    Private isformbeingdragged As Boolean = False
    Private mousedownx As Integer
    Private mousedowny As Integer

    Private Sub PictureBox25_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox25.MouseDown, Label13.MouseDown
        If e.Button = MouseButtons.Left Then
            isformbeingdragged = True
            mousedownx = e.X
            mousedowny = e.Y
        End If
    End Sub

    Private Sub PictureBox25_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox25.MouseUp, Label13.MouseUp
        If e.Button = MouseButtons.Left Then isformbeingdragged = False
    End Sub

    Private Sub Form1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox25.MouseMove, Label13.MouseMove
        If isformbeingdragged Then
            If Not Me.Location.Y + (e.Y - mousedowny) < 0 Then
                If Not Me.Location.Y + (e.Y - mousedowny) + 30 > My.Computer.Screen.WorkingArea.Height Then
                    Me.Location = New Point(Me.Location.X + (e.X - mousedownx), Me.Location.Y + (e.Y - mousedowny))
                Else
                    Me.Location = New Point(Me.Location.X + (e.X - mousedownx), My.Computer.Screen.WorkingArea.Height - 30)
                End If
            Else
                Me.Location = New Point(Me.Location.X + (e.X - mousedownx), 0)
            End If
        End If
    End Sub

    Private Sub Form1_Change() Handles MyBase.Resize
        Me.Refresh()
    End Sub

    Public counttime As New Stopwatch()

    Private Sub Form1_Load() Handles MyBase.Load
        Me.Visible = False
        Me.Opacity = 0
        Directory.SetCurrentDirectory(Application.StartupPath() & "\")
        Label1.Visible = False
        Label2.Visible = False
        Label3.Visible = False
        PictureBox1.Visible = False
        PictureBox2.Visible = False
        PictureBox14.Visible = False
        PictureBox5.Visible = True
        PictureBox6.Visible = True
        PictureBox7.Visible = True
        Me.Size = New System.Drawing.Size(600, 340)
        Process.Start(New Diagnostics.ProcessStartInfo("taskkill.exe", "/f /im adb.exe") With {.WindowStyle = Diagnostics.ProcessWindowStyle.Hidden}).WaitForExit()
        Process.Start(New Diagnostics.ProcessStartInfo("taskkill.exe", "/f /im ADB.exe") With {.WindowStyle = Diagnostics.ProcessWindowStyle.Hidden}).WaitForExit()
        Me.Visible = True
        Me.Activate()
        For i As Double = 0.0 To 1.0 Step 0.1
            Delay(0)
            Me.Opacity = i
        Next
        Me.Opacity = 1
        Me.Activate()
        Dim adbwaitfordevice As New Process()
        Dim adbsu As New Process()
adb:
        adbwaitfordevice.StartInfo.FileName = "adb.exe"
        adbwaitfordevice.StartInfo.Arguments = "shell """"echo 1"""""
        adbwaitfordevice.StartInfo.UseShellExecute = False
        adbwaitfordevice.StartInfo.CreateNoWindow = True
        adbwaitfordevice.StartInfo.RedirectStandardOutput = True
        adbwaitfordevice.Start()
        Do Until adbwaitfordevice.HasExited = True
            Delay(0)
        Loop
        Do Until "1" = adbwaitfordevice.StandardOutput.ReadLine()
            adbwaitfordevice.StandardOutput.DiscardBufferedData()
            adbwaitfordevice.Start()
            Delay(0.5)
        Loop
        adbsu.StartInfo.FileName = "adb.exe"
        adbsu.StartInfo.Arguments = "shell su -c 'echo rooted'"
        adbsu.StartInfo.UseShellExecute = False
        adbsu.StartInfo.CreateNoWindow = True
        adbsu.StartInfo.RedirectStandardOutput = True
        adbsu.Start()
        Do Until adbsu.HasExited = True
            Delay(0)
        Loop
        Dim rooted As Boolean = False
        If adbsu.StandardOutput.ReadLine().Contains("rooted") Then
            rooted = True
            PictureBox2.Visible = False
            PictureBox1.Image = My.Resources.connected
        End If
        Do Until PictureBox1.Visible = True
            Delay(0.1)
        Loop
        If rooted = False Then
            adbsu.StandardOutput.DiscardBufferedData()
            adbsu.StartInfo.Arguments = "shell if [ -a /system/xbin/su ]; then echo root; fi"
            adbsu.Start()
            Do Until adbsu.HasExited = True
                Delay(0)
            Loop
            If adbsu.StandardOutput.ReadLine().Contains("root") Then
                If language = 0 Then
                    MsgBox(Prompt:="Your device is not properly rooted" & vbNewLine & "or set to reject ADB's root permission." & vbNewLine & vbNewLine & "If so, please check your ADB settings.", Buttons:=vbCritical + vbOKOnly)
                Else
                    MsgBox(Prompt:="루팅이 올바르게 되어있지 않거나," & vbNewLine & "ADB 루트 엑세스를 해제한 기기입니다." & vbNewLine & vbNewLine & "ADB관련 설정을 확인하십시오.", Buttons:=vbCritical + vbOKOnly)
                End If
            Else
                adbsu.StandardOutput.DiscardBufferedData()
                adbsu.StartInfo.Arguments = "shell if [ -a /system/bin/su ]; then echo root; fi"
                adbsu.Start()
                Do Until adbsu.HasExited = True
                    Delay(0)
                Loop
                If adbsu.StandardOutput.ReadLine().Contains("root") Then
                    If language = 0 Then
                        MsgBox(Prompt:="Your device is not properly rooted" & vbNewLine & "or set to reject ADB's root permission." & vbNewLine & vbNewLine & "If so, please check your ADB settings.", Buttons:=vbCritical + vbOKOnly)
                    Else
                        MsgBox(Prompt:="루팅이 올바르게 되어있지 않거나," & vbNewLine & "ADB 루트 엑세스를 해제한 기기입니다." & vbNewLine & vbNewLine & "ADB관련 설정을 확인하십시오.", Buttons:=vbCritical + vbOKOnly)
                    End If
                End If
            End If
            PictureBox1.Image = My.Resources.notrooted
            adbwaitfordevice.StandardOutput.DiscardBufferedData()
            adbwaitfordevice.Start()
            Do While "1" = adbwaitfordevice.StandardOutput.ReadLine()
                adbwaitfordevice.StandardOutput.DiscardBufferedData()
                adbwaitfordevice.Start()
                Delay(0.25)
            Loop
            PictureBox1.Image = My.Resources.connectyourdevice
            GoTo adb
        End If
        adbsu.Dispose()
        adbwaitfordevice.Dispose()
        PictureBox2.Visible = False
        PictureBox1.Image = My.Resources.connected
        For i = 340 To 399 Step 2
            Me.Size = New System.Drawing.Size(600, i)
            Delay(0)
        Next
        Me.Size = New System.Drawing.Size(600, 399)
    End Sub

    Private Sub PictureBox4_MouseDown(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox4.MouseDown
        If stage = 3 Then
            If stage = 3 Then PictureBox4.Image = My.Resources.start_roll
        Else
            PictureBox4.Image = My.Resources.next_roll
        End If
    End Sub

    Private Sub PictureBox4_MouseUp(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox4.MouseUp
        If stage = 3 Then
            If stage = 3 Then PictureBox4.Image = My.Resources.start
        Else
            PictureBox4.Image = My.Resources._next
        End If
    End Sub

    Private Sub PictureBox2_MouseDown(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseDown
        PictureBox2.Image = My.Resources.help_roll
    End Sub

    Private Sub PictureBox2_MouseUp(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseUp
        PictureBox2.Image = My.Resources.help
    End Sub

    Private Sub PictureBox27_MouseDown(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox27.MouseDown
        PictureBox27.Image = My.Resources.minimize_roll
    End Sub

    Private Sub PictureBox27_MouseUp(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox27.MouseUp
        PictureBox27.Image = My.Resources.minimize
    End Sub

    Private Sub PictureBox28_MouseDown(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox28.MouseDown
        PictureBox28.Image = My.Resources.close_roll
    End Sub

    Private Sub PictureBox28_MouseUp(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles PictureBox28.MouseUp
        PictureBox28.Image = My.Resources.close
    End Sub

    Private Sub PictureBox27_Click(sender As Object, e As EventArgs) Handles PictureBox27.Click
        For i As Double = 1.0 To 0.0 Step -0.1
            Delay(0)
            Me.Opacity = i
        Next
        Me.WindowState = FormWindowState.Minimized
        Me.Opacity = 1
    End Sub

    Public ad_closing As Integer = 0

    Private Sub PictureBox28_MouseClick(sender As Object, e As MouseEventArgs) Handles PictureBox28.MouseClick
        If spinning = 1 Then Exit Sub
        If e.Button = Windows.Forms.MouseButtons.Left Then ad_close()
        If e.Button = Windows.Forms.MouseButtons.Right Then ad_restart()
    End Sub

    Private Sub ad_close()
        If ad_closing = 1 Then Exit Sub
        If Not Me.Opacity = 1 Then Exit Sub
        ad_closing = 1
        Process.Start(New Diagnostics.ProcessStartInfo("taskkill.exe", "/f /im adb.exe") With {.WindowStyle = Diagnostics.ProcessWindowStyle.Hidden}).WaitForExit()
        Process.Start(New Diagnostics.ProcessStartInfo("taskkill.exe", "/f /im ADB.exe") With {.WindowStyle = Diagnostics.ProcessWindowStyle.Hidden}).WaitForExit()
        For i As Double = 1.0 To 0.0 Step -0.1
            Delay(0)
            Me.Opacity = i
        Next
        Environment.Exit("0")
        Application.Exit()
        Application.ExitThread()
        Return
        Exit Sub
    End Sub

    Private Sub ad_restart()
        If language = 0 Then
            If MsgBox(Prompt:="Restart this application?", Buttons:=vbQuestion + vbYesNo) = vbYes Then
                For i As Double = 1.0 To 0.0 Step -0.1
                    Me.Opacity = i
                    Delay(0)
                Next
                Application.Restart()
                Environment.Exit("0")
                Application.Exit()
                Application.ExitThread()
                Return
            End If
        Else
            If MsgBox(Prompt:="재시작 하겠습니까?", Buttons:=vbQuestion + vbYesNo) = vbYes Then
                For i As Double = 1.0 To 0.0 Step -0.1
                    Me.Opacity = i
                    Delay(0)
                Next
                Application.Restart()
                Environment.Exit("0")
                Application.Exit()
                Application.ExitThread()
                Return
            End If
        End If
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
        If spinning = 1 Then Exit Sub
        Call ad_close()
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        If language = 0 Then
            MsgBox(Prompt:="The device which will be used for this application" & vbNewLine & "must be rooted and USB debugging enabled." & vbNewLine & vbNewLine & "If this application fails to recognize your device," & vbNewLine & "please check you have appropriate ADB drivers installed.", Buttons:=vbInformation + vbOKOnly)
        Else
            MsgBox(Prompt:="조각 모음에 사용될 기기는" & vbNewLine & "루팅이 되어있고 USB 디버깅이 활성화 되어있어야 합니다." & vbNewLine & vbNewLine & "기기 인식에 실패할 경우," & vbNewLine & "USB 디버깅(adb)드라이버가 올바르게 설치되어있는지 확인하십시오.", Buttons:=vbInformation + vbOKOnly)
        End If
    End Sub

    Public stage As Integer = 1
    Public language As Integer = 0
    Public spinning As Integer = 0

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        If stage = 3 Then
            PictureBox1.Image = My.Resources.running
            PictureBox4.Visible = False
            Call running()
            Exit Sub
        Else
            If stage = 1 Then
                If language = 0 Then PictureBox1.Image = My.Resources.knoweng
                If language = 1 Then PictureBox1.Image = My.Resources.knowkor
                stage = 2
                Exit Sub
            End If
            stage = 3
            If language = 0 Then PictureBox1.Image = My.Resources.happeneng
            If language = 1 Then PictureBox1.Image = My.Resources.happenkor
            PictureBox4.Image = My.Resources.start
            Exit Sub
        End If
    End Sub

    Public adbrun As Integer = 0
    Public WithEvents adb As New Process()

    Private Sub running()
        counttime.Start()
        spinning = 1
        Label1.Visible = True
        Label2.Visible = True
        If language = 0 Then Label1.Text = "(1/6) Sending defrag utility to the device"
        If language = 1 Then Label1.Text = "(1/6) 조각 모음 유틸리티를 기기에 전송"
        Dim titletext As String = Label13.Text
        adb.StartInfo.FileName = "adb.exe"
        adb.StartInfo.Arguments = "shell su -c 'mkdir /data/local/tmp'"
        adb.StartInfo.WorkingDirectory = (Application.StartupPath() & "\")
        adb.StartInfo.UseShellExecute = False
        adb.StartInfo.CreateNoWindow = True
        adb.StartInfo.RedirectStandardOutput = False
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        Label13.Text = titletext & " (0%)"
        If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(0, 100)
        adb.StartInfo.Arguments = "shell su -c 'rm -r /data/local/tmp/defrag'"
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        adb.StartInfo.Arguments = "shell su -c 'chmod 777 /data/local/tmp'"
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        adb.StartInfo.Arguments = "shell su -c 'mkdir /data/local/tmp/defrag'"
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        adb.StartInfo.Arguments = "shell su -c 'chmod 777 /data/local/tmp/defrag'"
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        adb.StartInfo.Arguments = "push defrag /data/local/tmp/defrag"
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        adb.StartInfo.Arguments = "shell su -c 'chmod 777 /data/local/tmp/defrag/*'"
        adb.Start()
        Do Until adb.HasExited = True
            Delay(0)
        Loop
        If language = 0 Then Label1.Text = "(2/6) zipaligning all installed applications"
        If language = 1 Then Label1.Text = "(2/6) 설치된 모든 어플리케이션 zipalign"
        PictureBox8.Visible = True
        For i = 1 To 5 Step 1
            PictureBox8.Location = New Point(-592 + i, 385)
            If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(i, 594)
            Delay(0.05)
        Next
        Label13.Text = titletext & " (1%)"
        adb.StartInfo.Arguments = "shell su -c '/data/local/tmp/defrag/run'"
        adb.StartInfo.RedirectStandardOutput = True
        adb.Start()
        adbrun = 1
        adb.BeginOutputReadLine()
        Do Until percentageedataall.Contains("zipalign finished")
            Delay(0.25)
        Loop
        If language = 0 Then Label1.Text = "(3/6) Defragging /system"
        If language = 1 Then Label1.Text = "(3/6) /system 조각 모음"
        For i = 1 To 10 Step 1
            PictureBox8.Location = New Point(-587 + i, 385)
            If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(5 + i, 594)
            Delay(0.05)
        Next
        Label13.Text = titletext & " (2%)"
        Do Until percentageedataall.Contains("system finished")
            For i = PictureBox8.Location.X To -577 + CInt(85 * (percentageedata.Substring(percentageedata.IndexOf("/") + 1) - percentageedata.Substring(0, percentageedata.IndexOf("/"))) \ percentageedata.Substring(percentageedata.IndexOf("/") + 1)) Step 1
                PictureBox8.Location = New Point(i, 385)
                Label13.Text = titletext & " (" & CInt((100 * (592 + i)) / 594) & "%)"
                If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(592 + i, 594)
                Delay(0.015)
            Next
            Delay(0.1)
        Loop
        If language = 0 Then Label1.Text = "(4/6) Defragging /data"
        If language = 1 Then Label1.Text = "(4/6) /data 조각 모음"
        Label2.Text = ""
        percentageedata = "100/100"
        Do Until percentageedataall.Contains("data finished")
            For i = PictureBox8.Location.X To -492 + CInt(200 * (percentageedata.Substring(percentageedata.IndexOf("/") + 1) - percentageedata.Substring(0, percentageedata.IndexOf("/"))) \ percentageedata.Substring(percentageedata.IndexOf("/") + 1)) Step 1
                PictureBox8.Location = New Point(i, 385)
                Label13.Text = titletext & " (" & CInt((100 * (592 + i)) / 594) & "%)"
                If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(592 + i, 594)
                Delay(0.015)
            Next
            Delay(0.1)
        Loop
        If language = 0 Then Label1.Text = "(5/6) Defragging /system #2"
        If language = 1 Then Label1.Text = "(5/6) /system 2차 조각 모음"
        Label2.Text = ""
        percentageedata = "100/100"
        Do Until percentageedataall.Contains("system2 finished")
            For i = PictureBox8.Location.X To -292 + CInt(100 * (percentageedata.Substring(percentageedata.IndexOf("/") + 1) - percentageedata.Substring(0, percentageedata.IndexOf("/"))) \ percentageedata.Substring(percentageedata.IndexOf("/") + 1)) Step 1
                PictureBox8.Location = New Point(i, 385)
                Label13.Text = titletext & " (" & CInt((100 * (592 + i)) / 594) & "%)"
                If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(592 + i, 594)
                Delay(0.015)
            Next
            Delay(0.1)
        Loop
        If language = 0 Then Label1.Text = "(6/6) Defragging /data #2"
        If language = 1 Then Label1.Text = "(6/6) /data 2차 조각 모음"
        Label2.Text = ""
        percentageedata = "100/100"
        Do Until percentageedataall.Contains("data2 finished")
            For i = PictureBox8.Location.X To -192 + CInt(186 * (percentageedata.Substring(percentageedata.IndexOf("/") + 1) - percentageedata.Substring(0, percentageedata.IndexOf("/"))) \ percentageedata.Substring(percentageedata.IndexOf("/") + 1)) Step 1
                PictureBox8.Location = New Point(i, 385)
                Label13.Text = titletext & " (" & CInt((100 * (592 + i)) / 594) & "%)"
                If osver >= 61 Then TaskbarManager.Instance.SetProgressValue(592 + i, 594)
                Delay(0.015)
            Next
            Delay(0.1)
        Loop
        Do Until adb.HasExited = True
            Delay(0.1)
        Loop
        adbrun = 0
        For i = 385 To 390 Step 1
            PictureBox8.Location = New Point(-6, i)
            Delay(0.05)
        Next
        PictureBox8.Visible = False
        Label1.Visible = False
        Label2.Visible = False
        Label13.Text = titletext
        If osver >= 61 Then TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress)
        spinning = 0
        For i = 399 To 340 Step -2
            Me.Size = New System.Drawing.Size(600, i)
            Delay(0)
        Next
        Me.Size = New System.Drawing.Size(600, 340)
        PictureBox1.Image = My.Resources.done
        counttime.Stop()
        If counttime.ElapsedMilliseconds > 60000 Then
            If language = 0 Then Label3.Text = CInt(counttime.ElapsedMilliseconds \ 60000) & "m : " & (counttime.ElapsedMilliseconds \ 1000) - CInt(counttime.ElapsedMilliseconds \ 60000) * 60 & "s"
            If language = 1 Then Label3.Text = CInt(counttime.ElapsedMilliseconds \ 60000) & "분 " & (counttime.ElapsedMilliseconds \ 1000) - CInt(counttime.ElapsedMilliseconds \ 60000) * 60 & "초"
        Else
            If language = 0 Then Label3.Text = Format(CDbl(counttime.ElapsedMilliseconds / 1000), "0.0") & " seconds"
            If language = 1 Then Label3.Text = Format(CDbl(counttime.ElapsedMilliseconds / 1000), "0.0") & "초"
        End If
        Label3.Visible = True
        PictureBox14.Visible = True
    End Sub

    Public percentageedataall As String = ""
    Public percentageedata As String = "100/100"

    Private Sub adbexited() Handles adb.Exited
        If Not percentageedataall.Contains("data2 finished") And adbrun = 1 Then
            If language = 0 Then MsgBox(Prompt:="Your device has been unplugged" & vbNewLine & vbNewLine & "or an unexpected error occurred." & vbNewLine & vbNewLine & vbNewLine & "Some data might have been lost.", Buttons:=vbCritical + vbOKOnly)
            If language = 1 Then MsgBox(Prompt:="기기 연결이 중간에 해제되었거나" & vbNewLine & vbNewLine & "오류가 발생하여 작업이 최소되었습니다." & vbNewLine & vbNewLine & vbNewLine & "데이터 손실이 발생했을 수도 있습니다.", Buttons:=vbCritical + vbOKOnly)
            Call ad_close()
        End If
    End Sub

    Private Sub percentagoutput(sender As Object, e As DataReceivedEventArgs) Handles adb.OutputDataReceived
        If Not String.IsNullOrEmpty(e.Data) And Not e.Data = Nothing Then
            percentageedataall += e.Data & vbNewLine
            If e.Data.Contains("/") Then
                percentageedata = e.Data
                label2update()
            End If
        End If
    End Sub

    Private Sub label2update()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf label2update))
        Else
            If language = 0 Then Label2.Text = CInt(percentageedata.Substring(0, percentageedata.IndexOf("/"))) & " files left out of " & CInt(percentageedata.Substring(percentageedata.IndexOf("/") + 1)) & " total files"
            If language = 1 Then Label2.Text = CInt(percentageedata.Substring(percentageedata.IndexOf("/") + 1)) & "개의 파일 중 " & CInt(percentageedata.Substring(0, percentageedata.IndexOf("/"))) & "개 남음"
        End If
    End Sub

    Private Sub PictureBox6_MouseHover(sender As Object, e As EventArgs) Handles PictureBox6.MouseEnter
        PictureBox6.Image = My.Resources.welcomeeng
    End Sub

    Private Sub PictureBox6_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox6.MouseLeave
        PictureBox6.Image = My.Resources.english
    End Sub

    Private Sub PictureBox7_MouseHover(sender As Object, e As EventArgs) Handles PictureBox7.MouseEnter
        PictureBox7.Image = My.Resources.welcomekor
    End Sub

    Private Sub PictureBox7_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox7.MouseLeave
        PictureBox7.Image = My.Resources.korean
    End Sub

    Private Sub PictureBox6_Click() Handles PictureBox6.Click
        PictureBox1.Visible = True
        PictureBox2.Visible = True
        PictureBox4.Visible = True
        PictureBox5.Visible = False
        PictureBox6.Visible = False
        PictureBox7.Visible = False
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        language = 1
        Call PictureBox6_Click()
    End Sub

End Class